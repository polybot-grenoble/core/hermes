# Hermès

![Header Hermès](./images/Header%20Hermès.png)

## Introduction

Hermès est un protocole de communication créé pour l'association de robotique
Polybot Grenoble de l'école d'ingénieurs Grenoble UGA INP Polytech Grenoble.

Ce protocole, initialement créé pour être exploité sur un bus CAN, peut être
utilisé sur n'importe quelle couche permettant de transmettre 12 octets ou plus
par trame. 

## Sommaire 

- [Hermès](#hermès)
  - [Introduction](#introduction)
  - [Sommaire](#sommaire)
  - [Importation](#importation)
    - [Constantes modifiables](#constantes-modifiables)
    - [CMake](#cmake)
    - [PlatformIO](#platformio)
  - [Fonctionnement](#fonctionnement)
    - [HermesBuffer](#hermesbuffer)
      - [HermesHeader](#hermesheader)
      - [HermesType](#hermestype)
    - [HermesBufferManager](#hermesbuffermanager)
    - [Transmission de données](#transmission-de-données)
      - [Interactions avec l'utilisateur](#interactions-avec-lutilisateur)
      - [Transmission d'un buffer simple](#transmission-dun-buffer-simple)
      - [Transmission partielle](#transmission-partielle)
  - [Modules complémentaires](#modules-complémentaires)
    - [HermesPeripheral](#hermesperipheral)
    - [Base64](#base64)
    - [HermesBridge](#hermesbridge)
  - [Exemples d'utilisation](#exemples-dutilisation)
    - [Utilisation par variables globales](#utilisation-par-variables-globales)
    - [Utilisation avec un objet](#utilisation-avec-un-objet)
    - [Utilisation du mode périphérique](#utilisation-du-mode-périphérique)
    - [Envoi simple d'une requête ou d'une réponse](#envoi-simple-dune-requête-ou-dune-réponse)
    - [Envoi manuel d'une requête ou d'une réponse](#envoi-manuel-dune-requête-ou-dune-réponse)


## Importation

Hermès n'a pas pour but d'être exécuté comme tel. Il s'agit d'une bibliothèque
statique, qui doit être intégrée dans un projet.

Pour cela, il est conseillé d'utiliser les sous-modules Git. 
Pour ajouter Hermès au Git de votre projet, utilisez la commande suivante :

```sh
git submodule add git@gitlab.com:polybot-grenoble/2023-2024/core/hermes.git <chemin>
```

Modifiez `<chemin>` par l'emplacement où vous voulez cloner Hermès. Par exemple,
pour un projet PlatformIO, vous remplaceriez par `./lib/hermes`. 
Nous vous conseillons d'utiliser le clonage par SSH plutôt que par HTTPS.

### Constantes modifiables 

Hermès possède quelques constantes configurables en fonction de l'application :

- `HERMES_NO_AUTO_HEARTBEAT` : Désactive la réponse automatique à une requête 
  de pouls.
- `HERMES_MAX_BUFFER_LEN` : Configure la longueur maximale du stockage des 
  arguments propres à un `HermesBuffer`. Il est conseillé de garder cette valeur
  constante sur un même "bus".
- `HERMES_BUFFER_COUNT` : Configure le nombre de `HermesBuffer` contenus dans 
  un `HermesBufferManager`. Cette valeur ne peut pas être supérieure à **255**.
- `HERMES_MAX_PAYLOAD` : Configure la taille maximale d'un fragment de donnée 
  que peut produire (ou recevoir) Hermès en direction (ou depuis) de son 
  support. Cette valeur doit être comprise **entre 12 et 65 535**. 

### CMake

Pour les projets n'utilisant pas PlatformIO, Hermès supporte CMake. 

La première étape est d'importer Hermès (en remplaçant `lib/hermes` par 
l'emplacement du sous-module) :

```cmake
add_subdirectory(lib/hermes)
```

Ensuite, pour chaque cible de compilation `truc` (library, executable, ...), 
il faut ajouter Hermès aux définitions d'inclusion, de lien et de "`-D`" :

```cmake
target_include_directories(truc PUBLIC 
  <...> 
  ${CMAKE_CURRENT_SOURCE_DIR}/lib/hermes/include
)
target_compile_definitions(truc PUBLIC 
  <...> 
  ${HERMES_COMPILE_DEFS}
)
target_link_libraries(truc PUBLIC 
  <...>
  PolybotHermes
)
```

Ici `<...>` représente les définitions déjà présentes, s'il n'y en a pas, il ne
faut rien mettre. Encore une fois, `lib/hermes` doit être remplacé par 
l'emplacement du sous module.

Les constantes d'Hermès peuvent être éditées avant `add_library`, ou bien 
graphiquement avant la compilation via `cmake-gui`.

Pour importer Hermès dans votre code, il vous suffit d'écrire :

```c++
// En C
#include <Hermes.h>

// En C++
extern "C" {
  #include <Hermes.h>
}
```

### PlatformIO

Pour utiliser Hermès dans PlatformIO, le sous-module doit être cloné dans 
`lib/hermes`. Cela permet au compilateur de détecter automatiquement sa 
présence. 

Si vous voulez modifier les constantes, éditez votre fichier `platformio.ini` :

```ini
[env:cible]
...
build_flags =
    -DHERMES_MAX_PAYLOAD=64u
    ...
```

Ici, `cible` est la carte ciblée, `...` les paramètres déjà définis. 
Les constantes doivent être préfixées par `-D` sans espaces.

Pour importer Hermès dans votre code, il vous suffit d'écrire :

```c++
// En C
#include <Hermes.h>

// En C++
extern "C" {
  #include <Hermes.h>
}
```

## Fonctionnement

Hermès est pensé pour une utilisation sur un réseau avec une typologie 
de bus. Chaque instance d'Hermès peut communiquer directement à une autre. 
Ces instances seront appelées des **périphériques**. Chaque périphérique 
possède un **identifiant** codé sur un octet. 

![Topologie](./images/Topologie.png)

Chaque périphérique peut définir **4 080 commandes**. Un paquet transmis entre 
deux périphériques est appelé **buffer**. Un buffer peut être considéré comme 
une **requête**, c'est-à-dire un **appel** à une commande d'un périphérique, ou 
comme une **réponse**, à une requête.

Chaque commande peut nécessiter un nombre arbitraire d'**arguments** et avoir
un nombre arbitraire de valeurs de retour. La nature de chacun de ces arguments
est codé par un caractère définissant son **type**. L'ensemble de ces types
forme la **signature** d'un buffer. 

Toute requête ne crée pas de réponse si celle-ci est considérée comme inutile
par l'utilisateur. Hermès suppose donc que le support est suffisamment fiable 
et qu'aucune transmission ne sera perdue lors d'un appel.

Si la taille de la **charge utile** d'un buffer est trop volumineuse pour
être transmise sur le support, Hermès découpe le **buffer** et effectue une
**transmission partielle**. Le périphérique récepteur se charge de la 
reconstitution du buffer à la réception des fragments.

### HermesBuffer

Un **buffer** consiste en un en-tête suivi d'un tableau d'octets de longueur
arbitraire. Ce tableau possède une taille maximale définie par la constante 
`HERMES_MAX_BUFFER_LEN`, définie comme `1000u (unsigned)` par défaut. 

![HermesBuffer](./images/Buffer.png)

#### HermesHeader

L'en-tête du buffer contient les informations suivantes : 

- `remote` L'identifiant de périphérique du récepteur ;
- `command` L'identifiant de commande ;
- `sender` L'identifiant de périphérique de l'envoyeur ;
- `req_res` Un drapeau permettant de différencier une requête d'une réponse.

Hermès étant initialement prévu pour être utilisé sur un bus CAN 2.0B, 
cet en-tête sera sérialisé sous la forme d'un entier 29-bits avant envoi. 

![HermesHeader](./images/Header.png)

Lors d'un échange requête/réponse, les champs `remote` et `sender` vont être 
inversés. Cet échange doit être effectué par l'utilisateur. Par exemple, soient A et B deux périphériques, on aura pour une 
requête de commande C : `buffer.head = { B, C, A, 0 }` et la réponse 
correspondante sera de la forme `buffer.head = { A, C, B, 1 }`. 

_**NB :** Il est recommandé d'utiliser les abstractions de haut-niveau plutôt que de 
modifier l'en-tête manuellement
([`HermesBuffer_configure`](./include/HermesBuffer.h#L97),
[`Hermes_request`](./include/Hermes.h#L141),
[`Hermes_respond`](./include/Hermes.h#L157))._

#### HermesType

Les données transmises via un buffer sont codées selon les types fondamentaux 
du C. Plus précisément, ce sont les types standards pour les architectures 
petit-boutistes. Chaque donnée est copiée depuis son emplacement mémoire dans 
le buffer. Pour pouvoir discerner ces données, elles sont préfixées par un 
octet dit de type. Un type suivi d'une donnée donne un argument.

Lorsqu'on met les indicateurs de type à la suite sous forme de chaine de 
caractères, on obtient la signature du buffer. Cette signature est utilisée 
par les fonctions [`Hermes_request`](./include/Hermes.h#L141) et
[`Hermes_respond`](./include/Hermes.h#L157) pour déterminer le nombre de 
paramètres passés (comme un `printf`).

![Signature](./images/Signature.png)

La liste des types disponibles est :

| Indicateur | Type en C  | Taille (octets) |
| :--------: | :--------: | :-------------: |
| `0`        | `void`     | 0               |
| `c`        | `char`     | 1               |
| `o`        | `int8_t`   | 1               |
| `x`        | `int16_t`  | 2               |
| `i`        | `int32_t`  | 4               |
| `l`        | `int64_t`  | 8               |
| `f`        | `float`    | 4               |
| `d`        | `double`   | 8               |
| `s`        | `char*`    | min : 1         |
| `b`        | `uint8_t*` | min : 0         |

Les types `s` et `b` sont particuliers. Ils possèdent tous deux une taille 
minimale, mais peuvent changer de taille en fonction de ce qu'ils doivent 
stocker. Les fonctions
[`HermesBuffer_set`](./include/HermesBuffer.h#L162) et 
[`HermesBuffer_setBlob`](./include/HermesBuffer.h#L186) s'occupent de décaler
les données et de modifier la taille du buffer en fonction de la valeur donnée.
 
![Arguments de taille variable](./images/Exemple%20String.png)

_**NB :** Un argument de type `b` ne peut pas être suivi par un autre argument._

_**NB :** Un buffer contient toujours un argument de type `0` pour terminer 
la détection de signature. Cet argument n'est pas obligatoirement compté dans
la taille d'un buffer._

_**NB :** Dans le code source, l'argument de type `b` est appelé **"blob"** pour 
"**B**inary **L**arge **OB**ject", du fait qu'il s'agisse d'une suite d'octets 
arbitraire. Il serait donc plus juste de dire qu'il correspond à un `void*`._

### HermesBufferManager

Hermès ayant pour cible première les microcontrôleurs, il n'utilise pas 
d'allocation de mémoire dynamique. Les **gestionnaires de buffer** servent donc
à gérer le stockage temporaire des buffers à envoyer ou reçus. Le nombre de
buffers gérés par gestionnaire (*manager* dans le code) est déterminé par la constante 
`HERMES_BUFFER_COUNT`, définie comme `8u` par défaut. 

Le gestionnaire associe à chaque buffer un ensemble de paramètres, qui sont :

- `rw_pos` : La position de la tête de lecture dans le buffer (en cas d'envoi),
  ou la longueur des données reçues (en cas de réception d'une transmission
  partielle) ;
- `ready` : Drapeau permettant de déterminer si le buffer doit être envoyé ou 
  que sa réception est complète ;
- `key` : Clé de transmission partielle associée au buffer. Si cette clé est 
  `>= 4`, alors le buffer n'utilise pas la transmission partielle.
- `segment` : Longueur d'un segment de buffer envoyé/reçu par transmission 
  partielle.

![BufferManager](./images/BufferManager.png)

Le tableau `allocated` permet de stocker l'ordre d'envoi ou de réception des 
buffers, en stockant les indices de ceux-ci dans le tableau `buffers`.

### Transmission de données

La capacité maximale de transmission sur le bus sera définie dans la
variable `hermes.defaultPayloadLength`. Cette valeur peut être ignorée 
en réimplémentant intégralement le mécanisme de transmission défini ci-après.

Pour faciliter l'implémentation d'un nouveau support, Hermès produit des 
**charges utiles** ayant une longueur inférieure à 
`hermes.defaultPayloadLength`. Ces charges utiles sont de simples tableaux 
d'octets. Les 4 premiers contiennent toujours l'identifiant de buffer sérialisé,
sous la forme d'un entier 32-bits petit-boutiste. Le bon fonctionnement 
d'Hermès suppose qu'une charge utile pleine fait au moins **12 octets**. 

![HermesPayload](./images/Hermes-Payload.png)

Hermès possède deux gestionnaires de buffer, un pour les buffers entrants 
(`input`) et un pour les buffers sortants (`output`).

#### Interactions avec l'utilisateur

Hermès utilise des callbacks pour gérer les différents "évènements" liés à son
utilisation. Ces callbacks sont appelés pendant un appel de 
[`Hermes_handle`](./include/Hermes.h#L190). On a :

- `logCallback` qui est utilisé lors de la réception d'un buffer de commande
  `HermesSysCmd_Log` ;
- `send` qui est appelée par Hermès pour envoyer des données sur le support ;
- `onMessage` qui est appelé par Hermès lors de la réception d'un buffer dont 
  l'identifiant de commande est inférieur à `HermesSysCmd` ;
- `onSystemMessage` qui est appelé par Hermès lors de la réception d'un buffer 
  dont l'identifiant de commande est supérieur à `HermesSysCmd`.

![Appel_Handle](./images/Appel_handle.png)

Pour générer un évènement d'envoi, il suffit d'allouer un buffer dans `output`,
le configurer, puis le marquer comme prêt 
(voir les [Exemples d'utilisation](#exemples-dutilisation)). 

#### Transmission d'un buffer simple

Dans le cas où la longueur du buffer et celle de son en-tête sérialisé
ne dépassent pas la capacité de transmission maximale du bus 
(`buffer.length + sizeof(uint32_t) < hermes.defaultPayloadLength`),
le buffer peut être transmis intégralement en un message sur le bus.

Lors de l'appel de la fonction [`Hermes_handle`](./include/Hermes.h#L190),
Hermès vérifie la présence de buffers marqués comme prêts dans `output`.
Tout buffer simple est transformé en charge utile, où on ajoute 4 à la 
longueur du buffer pour prendre en compte l'identifiant 29-bits. Cette
charge utile est ensuite envoyée via le callback. 

![EnvoiBuffer](./images/Envoi-simple.png)

Pour retrouver le buffer envoyé, le récepteur n'a plus qu'à appliquer 
l'opération inverse en le rangeant dans `input`. Ce buffer sera immédiatement
marqué comme prêt.

![ReceptionBuffer](./images/Reception-simple.png)

#### Transmission partielle

Lorsque la taille d'un buffer dépasse la capacité de transmission maximale du 
bus, il est envoyé par segments. L'algorithme d'envoi est appelé **transmission
partielle**. En résumé, Hermès signale au périphérique récepteur qu'il doit
allouer un emplacement pour recevoir un buffer, puis il envoie successivement 
les segments de buffer. Ce dernier est recomposé par le récepteur au sein de l'espace 
alloué. 

Pour indiquer au récepteur d'allouer un emplacement, Hermès attribue une clé
de transmission au buffer et envoie un buffer `HermesSysCmd_PartialBegin` ayant
pour signature `xxo`. Il contient l'identifiant de commande, la longueur du 
buffer à recevoir et la longueur maximale d'un segment de données envoyé lors de 
cette transmission partielle. La clé sert à éviter les collisions entre des
transmissions aux paramètres similaires, elle est stockée dans la partie haute 
de l'identifiant de commande pour des raisons d'optimisation. 

![PartialBegin](./images/PartialBegin.png)

Chaque segment est ensuite envoyé à l'aide d'un buffer 
`HermesSysCmd_PartialContent`, cet identifiant subit un "ou" logique avec la 
clé de transmission. Il y a donc 4 variantes possibles pour 
`HermesSysCmd_PartialContent` : `0xFFC`, `0xFFD`, `0xFFE` et `0xFFF`.
La signature du buffer est `b` pour optimiser la quantité d'informations
transmises. On a (dans l'ordre) : l'identifiant de commande (12 bits), 
le numéro du segment (12 bits) et le segment de données (1 à 4 octets).

Le segment est déterminé par la longueur de segment et la position initiale
indiquée par `rw_pos`. A chaque envoi, on incrémente `rw_pos` de la longueur
de segment jusqu'à ce que le buffer soit entièrement lu. Une fois fini,
le buffer est libéré. On détermine ainsi pour chaque envoi le numéro de 
segment à partir de `rw_pos`.

![PartialContent](./images/PartialContent.png)

La reconstitution se fait au fur et à mesure de la réception. Si on note 
`s` la longueur de segment, `t` le numéro de segment et `d` les données,
on a `buffer.data[s*t] = d`. On utilise le champ `rw_pos` associé à 
l'emplacement de réception pour compter le nombre d'octets reçus. Une fois
que `rw_pos` est égal à la longueur du buffer envoyé, on marque le buffer
reçu comme prêt. 

![TransmissionPartielle](./images/TransmissionPartielle.png)

## Modules complémentaires

Cette partie décrit les modules complémentaires. Ils peuvent aider à 
l'implémentation d'un périphérique ou à l'adaptation à un bus de données.

### HermesPeripheral

...

### Base64

...

### HermesBridge

...

## Exemples d'utilisation

Quelques exemples d'utilisation génériques. 

### Utilisation par variables globales

```c
/* Cet exemple est écrit en C */
#include <Hermes.h>
#include <stdio.h>

/** On imagine qu'il existe un élément dans le programme qui génère les 
 *  données à lire. Les définitions ci-dessous correspondent donc à la 
 *  lecture et à l'écriture des données sur le Bus. 
 */
bool dataAvailable ();
HermesPayload read ();
void write (HermesPayload data);

/** Cette fonction est le callback qui gère la réception des buffers */
void onMessage (Hermes_t* hermes, HermesBuffer* buffer) {

  if (buffer->head.req_res) {
    // C'est un buffer de type "réponse"
    ...
    return;
  }

  // C'est un buffer de type "requête"
  switch (buffer->head.command) {
    case 1:
      ...
      break;
    ...
  }

}

/** Cette fonction gère la réception des messages de Log */
void onLog (
  Hermes_t* hermes, 
  HermesHeader head, const char* message, uint8_t errorCode
) {

  if (errorCode) {
    printf("Erreur %d : ", errorCode);
  }

  printf("[%03x] %s", head.sender, message);

}

/** Cette fonction gère l'envoi vers le bus */
void send (Hermes_t* hermes, HermesPayload payload) {

  write(payload);

}

/** Point d'entrée du programme */
int main () {

  // static permet d'économiser de la place sur la pile
  static Hermes_t Hermes, *hermes = &Hermes;

  // Initialisation d'Hermès
  Hermes_init(hermes);

  // Ajout des callbacks
  Hermes.send = &send;
  Hermes.logCallback = &onLog;
  Hermes.onMessage = &onMessage;

  // Boucle principale
  for (;;) { // <- Correspond à "loop" dans Arduino
    // Lecture du bus
    if (dataAvailable()) {
      Hermes_input(read()); 
    }
    // On laisse Hermès faire son travail
    Hermes_handle();
  }

  return 0;

}
```

### Utilisation avec un objet

...

### Utilisation du mode périphérique

...

### Envoi simple d'une requête ou d'une réponse

Envoi d'une requête de commande 5 au périphérique 12, qui nécessite un 
entier 8 bits et une chaine de caractères. Ici on envoie un octet représentant
le chiffre 7 et une chaîne de caractères "coucou". La signature sera donc "os".


```c
Hermes_request(12, 5, "os", 7, "coucou");
```

### Envoi manuel d'une requête ou d'une réponse

Envoi d'un tableau d'entiers au périphérique 13 par la commande 6, en 
utilisant le type arbitraire. Cette méthode est déconseillée mais nécessaire
pour l'envoi de types non pris en charge nativement par Hermès.

```c
typedef struct { uint8_t len; uint8_t data[8] } Tableau;
...

Hermes_t *hermes;

Tableau T;
T.len = 3
T.data[0] = 1;
T.data[1] = 2;
T.data[2] = 3;

HermesBuffer* buffer = HermesBufferManager_allocBuffer(&hermes->output);
HermesBuffer_configure(buffer, 13, 6, false, "b");
HermesBuffer_setBlob(buffer, 0, &T, sizeof(Tableau));
HermesBufferManager_markAsReady(&hermes->output, buffer);
```

## Crédits
>Logiciel développé par Julien Pierson
>
>Relecture par Jonathan Dumaz