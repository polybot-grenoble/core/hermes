#include "peripheralMethods.h"
#include <string.h>

static const HermesHandler HermesDefault_system[] = {
    { 
        HermesSysCmd_Manifest, "man", "", 
        (HermesMethod)HermesPeripheral_sendManifest, "xx", 
        false, 0 
    }
};

void HermesPeripheral_setup (
    Hermes_t* hermes, 
    const char* name,
    const HermesHandler* handlers, 
    uint16_t handlerCount
) {

    HermesPeripheral temp = {
        .handlerCount = handlerCount,
        .handlers = handlers,
        .name = name,
        .sysHandlerCount = sizeof(HermesDefault_system) / sizeof(HermesHandler),
        .sysHandlers = HermesDefault_system
    };

    hermes->peripheral      = temp;
    hermes->onMessage       = HermesPeripheral_handle;
    hermes->onSystemMessage = HermesPeripheral_handle;

}

void HermesPeripheral_handle (
    Hermes_t* hermes,  
    HermesBuffer* buffer
) {

    HermesHeader head = buffer->head;
    static char signature[1 + HERMES_MAX_BUFFER_LEN / 2] = { 0 };

    HermesBuffer_getSignature(buffer, signature);

    // Vérification du récepteur
    if (head.remote != hermes->id) {
        // Erreur silencieuse
        return;
    }

    uint16_t count = (head.command >= HermesSysCmd) 
        ? hermes->peripheral.sysHandlerCount 
        : hermes->peripheral.handlerCount;

    const HermesHandler *list = (head.command >= HermesSysCmd)
        ? hermes->peripheral.sysHandlers
        : hermes->peripheral.handlers;

    // Recherche de la fonction correspondante
    for (uint16_t i = 0; i < count; i++) {

        HermesHandler handle = list[i];

        // Vérification du pointeur
        if (!handle.callback) {
            continue;
        }
        
        // Vérification de l'identifiant de commande
        if (head.command != handle.command) {
            continue;
        }

        // Vérification du type de paquet
        if (head.req_res != handle.response) {
            continue;
        }

        // Vérification de l'envoyeur    
        if (handle.response && head.sender != handle.from) {
            continue;
        }

        // Vérification de la signature 
        if (strcmp(signature, handle.signature) != 0) {
            continue;
        }

        // Appel du callback
        handle.callback(hermes, buffer);
        return;

    }

    // Erreur : pas possible de gérer le paquet
    if (head.command < HermesSysCmd) {
        Hermes_respond(hermes, head.sender, HermesSysCmd_Error, "o", 0xff);
    }

}


size_t HermesManifest_length (Hermes_t* hermes) {

    size_t len = 0;

    // Longueur du nom
    len += 1 + strlen(hermes->peripheral.name);

    // Longueur du contenu
    for (uint16_t i = 0; i < hermes->peripheral.handlerCount; i++) {

        HermesHandler handle = hermes->peripheral.handlers[i];
        len += sizeof(HermesCommand) + 3 + strlen(handle.name) 
            +  strlen(handle.signature) + strlen(handle.outSignature);
        
    }

    return len;

}

HermesManifest HermesPeripheral_manifest (Hermes_t* hermes) {

    HermesManifest man;
    man.length      = HermesManifest_length(hermes);
    man.methods     = hermes->peripheral.handlerCount;
    man.manifest    = (uint8_t*)malloc(man.length * man.length);

    uint8_t *pos = man.manifest;
    size_t len;

    // Copie du nom
    len = 1 + strlen(hermes->peripheral.name);
    memcpy(pos, hermes->peripheral.name, len);
    pos += len;

    // Copie des méthodes
    for (uint16_t i = 0; i < hermes->peripheral.handlerCount; i++) {
        
        HermesHandler handle = hermes->peripheral.handlers[i];
        *(HermesCommand*)pos = handle.command;
        pos += sizeof(HermesCommand);

        len = 1 + strlen(handle.name);
        memcpy(pos, handle.name, len);
        pos += len;

        len = 1 + strlen(handle.signature);
        memcpy(pos, handle.signature, len);
        pos += len;

        len = 1 + strlen(handle.outSignature);
        memcpy(pos, handle.outSignature, len);
        pos += len;
        
    }

    return man;

}

void HermesPeripheral_sendManifest (Hermes_t* hermes, HermesBuffer* buffer) {

    HermesManifest man = HermesPeripheral_manifest(hermes);

    // En-tête
    HermesBuffer* buf = HermesBufferManager_allocPrioritizedBuffer(
        &hermes->output
    );

    HermesBuffer_configure(
        buf, buffer->head.sender, HermesSysCmd_Manifest, true, "xx"
    );
    HermesBuffer_set(buf, 0, &man.length);
    HermesBuffer_set(buf, 1, &man.methods);

    HermesBufferManager_markAsReady(&hermes->output, buf);

    // Corps
    buf = HermesBufferManager_allocBuffer(&hermes->output);
    HermesBuffer_configure(
        buf, buffer->head.sender, HermesSysCmd_ManifestChunk, true, "b"
    );

    /// Tronquage au cas où
    size_t len = man.length;
    if (len > HERMES_MAX_BUFFER_LEN - 1) {
        len = HERMES_MAX_BUFFER_LEN - 1;
    }

    HermesBuffer_setBlob(buf, 0, len, man.manifest);
    HermesBufferManager_markAsReady(&hermes->output, buf);

}

