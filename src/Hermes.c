#include "Hermes.h"

#include <stdlib.h>
#include <string.h>

void Hermes_init (Hermes_t* hermes, HermesID id) {

    hermes->id = id;

    hermes->send = NULL;
    hermes->onMessage = NULL;
    hermes->onSystemMessage = NULL;
    hermes->logCallback = NULL;

    hermes->partialKey = 0;
    hermes->defaultPayloadLength = HERMES_MAX_PAYLOAD;

    HermesBufferManager_init(&hermes->input);
    HermesBufferManager_init(&hermes->output);

}


bool Hermes_input (Hermes_t *hermes, HermesPayload payload) {
    
    // ID 29-bits
    uint32_t id = *(uint32_t*)payload.data;
    // En-tête du buffer
    HermesHeader head = HermesHeader_parse(id);
    // Pointeur vers les données à copier
    uint8_t* data = payload.data + sizeof(uint32_t);
    // Longueur des données à copier
    uint16_t dataLen = payload.length - sizeof(uint32_t);
    
    // Buffer à remplir
    HermesBuffer *buffer;
    // Indice du buffer à remplir
    uint8_t bufferIndex;
    
    // N° de commande
    uint16_t cmd;
    // Longueur du buffer
    uint16_t len;
    // Clé de transmission partielle
    uint8_t key;
    // Longueur des segments de données partielles pour un buffer donné
    uint8_t partialLen;

    // Le buffer est "normal"
    if (head.command < HermesSysCmd_PartialBegin) {
        // On alloue un buffer
        if (head.command >= HermesSysCmd) {
            buffer = HermesBufferManager_allocPrioritizedBuffer(&hermes->input);
        } else {
            buffer = HermesBufferManager_allocBuffer(&hermes->input);
        }
        if (!buffer) {
            return false;
        }
        bufferIndex = HermesBufferManager_indexOf(&hermes->input, buffer);

        // On copie le contenu
        buffer->head = head;
        memcpy(buffer->data, data, dataLen);
        buffer->length = dataLen;
        if (buffer->length < HERMES_MAX_BUFFER_LEN) {
            buffer->data[buffer->length] = HermesT_none;
        }

        // On marque le buffer comme prêt
        hermes->input.states[bufferIndex].ready = true;

        return true;
    }

    // Le buffer est un "Partial Begin"
    if (head.command == HermesSysCmd_PartialBegin) {
        // On récupère les paramètres
        cmd = *(uint16_t*)(data + 1);
        key = cmd >> 12;
        cmd &= 0xfff;
        len = *(uint16_t*)(data + 2 + sizeof(uint16_t));	
        partialLen = *(uint8_t*)(data + 3 + 2 * sizeof(uint16_t));

        // On alloue un buffer
        if (cmd >= HermesSysCmd) {
            buffer = HermesBufferManager_allocPrioritizedBuffer(&hermes->input);
        } else {
            buffer = HermesBufferManager_allocBuffer(&hermes->input);
        }

        if (!buffer) {
            return false;
        }
        bufferIndex = HermesBufferManager_indexOf(&hermes->input, buffer);

        // On paramètre le buffer
        buffer->head = head;
        buffer->head.command = cmd;
        buffer->length = len;
        if (buffer->length < HERMES_MAX_BUFFER_LEN) {
            buffer->data[buffer->length] = HermesT_none;
        }

        // On définit la clé
        hermes->input.states[bufferIndex].key = key;
        hermes->input.states[bufferIndex].segment = partialLen;

        return true;
    }
    

    // Le buffer est un "Partial Content"
    // Lecture des paramètres
    key = head.command & 3;
    uint16_t info_hi = *(uint16_t*)(data + 1);
    uint8_t  info_lo = *(data + 1 + sizeof(uint16_t));
    cmd = info_hi >> 4;
    // Numéro d'ordre de paquet
    uint16_t t = (info_hi & 0xf) | info_lo;	
    
    // On récupère le buffer nécessaire
    bufferIndex = HermesBufferManager_findRXPartial(
        &hermes->input, head.sender, cmd, key
    );
    if (bufferIndex == HERMES_BUFFER_COUNT) {
        return false;
    }
    buffer = &hermes->input.buffers[bufferIndex];

    // On ajoute le contenu au buffer
    uint8_t paramsLen = (1 + sizeof(uint16_t) + 1);
    uint8_t* content = (data + paramsLen);
    uint16_t contentLen = dataLen - paramsLen;
    uint16_t bufferOffset = t * hermes->input.states[bufferIndex].segment;
    memcpy(&buffer->data[bufferOffset], content, contentLen);
    
    // On avance "rw_pos" du nombre d'octets reçus 
    // (c'est plus une position mais une quantité, yolo !)
    hermes->input.states[bufferIndex].rw_pos += contentLen;
    
    // On vérifie si on a TOUT
    if (hermes->input.states[bufferIndex].rw_pos == buffer->length) {
        hermes->input.states[bufferIndex].ready = true;
    }	

    return true;

}

HermesPayload Hermes_output (Hermes_t *hermes, uint16_t maxLen) {

    if (maxLen < 12) {
        maxLen = 12;
    }
    else if (maxLen > HERMES_MAX_PAYLOAD) {
        maxLen = HERMES_MAX_PAYLOAD;
    }
    else if (maxLen > 250) {
        maxLen = 250;
    }

    uint16_t contentLen = maxLen - sizeof(uint32_t);
    uint16_t partialLen = contentLen - 4 * sizeof(uint8_t);
    uint16_t copyLen;

    HermesPayload out;
    out.length = 0;
    *(uint32_t*)out.data = 0u;

    uint8_t count = HermesBufferManager_readyBufferCount(&hermes->output);

    if (!count) {
        return out;
    }

    // On récupère le premier buffer
    uint8_t id = HermesBufferManager_nextBuffer(&hermes->output);
    HermesBuffer *buffer = &hermes->output.buffers[id];

    // Peut-on envoyer le buffer en 1 seul message ?
    if (buffer->length <= contentLen) {
        // On transmet le buffer entier        
        *(uint32_t*)out.data = HermesHeader_serialize(buffer->head);
        out.length += sizeof(uint32_t);
        
        // On calcule la longueur de la copie
        copyLen = buffer->length;
        memcpy(out.data + sizeof(uint32_t), buffer->data, copyLen);
        out.length += copyLen;

        // On libère le buffer
        HermesBufferManager_freeFirstBuffer(&hermes->output);

        // On envoie
        return out;

    }

    // On doit effectuer une transmission partielle
    HermesBuffer partial;
    HermesBuffer_clear(&partial);

    // Doit-on assigner une clé ?
    if (hermes->output.states[id].key >= 4) {
        // On assigne une clé
        hermes->output.states[id].key = hermes->partialKey;
        hermes->partialKey = (hermes->partialKey + 1) % 4;

        // On crée le PartialBegin
        partial.head = buffer->head;
        partial.head.command = HermesSysCmd_PartialBegin;
        
        uint16_t cmd = buffer->head.command | 
                       (hermes->output.states[id].key << 12);
        
        HermesBuffer_setSignature(&partial, "xxo");
        HermesBuffer_set(&partial, 0, &cmd);
        HermesBuffer_set(&partial, 1, &buffer->length);
        HermesBuffer_set(&partial, 2, &partialLen);

        // On envoie le PartialBegin
        out.length = sizeof(uint32_t) + partial.length;
        *(uint32_t*)out.data = HermesHeader_serialize(partial.head);
        memcpy(out.data + sizeof(uint32_t), partial.data, partial.length);

        return out;
    }

    // On crée un PartialContent

    uint16_t pos = hermes->output.states[id].rw_pos;
    uint16_t t = pos / partialLen;
    uint16_t settings_hi = (buffer->head.command << 4) | ((t >> 8) & 0xf);
    uint8_t settings_lo = t & 0xff;

    partial.head = buffer->head;
    partial.head.command = HermesSysCmd_PartialContent   | 
                           hermes->output.states[id].key ;
    HermesBuffer_setSignature(&partial, "b");
    
    // Renseignement manuel
    uint8_t* d = &partial.data[1];
    memcpy(d, &settings_hi, sizeof(uint16_t));
    d += sizeof(uint16_t);
    memcpy(d, &settings_lo, sizeof(uint8_t));
    d += sizeof(uint8_t);

    uint8_t tempLen = buffer->length - t * partialLen;
    if (tempLen < partialLen) {
        partialLen = tempLen;
    }

    memcpy(d, &buffer->data[pos], partialLen);
    hermes->output.states[id].rw_pos += partialLen;
    d += partialLen;

    partial.length = d - partial.data;

    // On envoie le PartialContent
    out.length = 2 * sizeof(uint16_t) + partial.length;
    *(uint32_t*)out.data = HermesHeader_serialize(partial.head);
    memcpy(out.data + sizeof(uint32_t), partial.data, partial.length);


    // On vérifie si on doit libérer le buffer
    if (hermes->output.states[id].rw_pos == buffer->length) {
        HermesBufferManager_freeFirstBuffer(&hermes->output);
    }

    return out;

}


bool Hermes_request(
    Hermes_t* hermes,
    HermesID remote, HermesCommand cmd, const char* signature,
    ...
) {

    HermesBuffer* buffer = HermesBufferManager_allocBuffer(&hermes->output);
    if (buffer == NULL) {
        return false;
    }

    HermesBuffer_configure(buffer, remote, cmd, false, signature);
    buffer->head.sender = hermes->id;

    va_list arg;
    va_start(arg, signature);

    uint16_t count = HermesBuffer_argCount(buffer);
    for (uint16_t i = 0; i < count; i++) {

        switch (signature[i])
        {
        case HermesT_char: {
            char c = (char) va_arg(arg, int);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int8: {
            int8_t c = (int8_t) va_arg(arg, int);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int16: {
            int16_t c = (int16_t) va_arg(arg, int);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int32: {
            int32_t c = (int32_t) va_arg(arg, int);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int64: {
            int64_t c = (int64_t) va_arg(arg, long int);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_double: {
            double c = (double) va_arg(arg, double);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_float: {
            float c = (float)va_arg(arg, double);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_string: {
            char* c = (char*)va_arg(arg, char*);
            HermesBuffer_set(buffer, i, c);
            break;
        }
        // Blob n'est pas accepté ici
        default:
            break;
        }

    }

    va_end(arg);

    HermesBufferManager_markAsReady(&hermes->output, buffer);

}


bool Hermes_respond(
    Hermes_t* hermes,
    HermesID remote, HermesCommand cmd, const char* signature,
    ...
) {

    HermesBuffer* buffer = HermesBufferManager_allocBuffer(&hermes->output);
    if (buffer == NULL) {
        return false;
    }

    HermesBuffer_configure(buffer, remote, cmd, true, signature);
    buffer->head.sender = hermes->id;

    va_list arg;
    va_start(arg, signature);

    uint16_t count = HermesBuffer_argCount(buffer);
    for (uint16_t i = 0; i < count; i++) {

        switch (signature[i])
        {
        case HermesT_char: {
            char c = (char)va_arg(arg, int);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int8: {
            int8_t c = (int8_t)va_arg(arg, int);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int16: {
            int16_t c = (int16_t)va_arg(arg, int);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int32: {
            int32_t c = (int32_t)va_arg(arg, int);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_int64: {
            int64_t c = (int64_t)va_arg(arg, long int);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_double: {
            double c = (double)va_arg(arg, double);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_float: {
            float c = (float)va_arg(arg, double);
            HermesBuffer_set(buffer, i, &c);
            break;
        }
        case HermesT_string: {
            char* c = (char*)va_arg(arg, char*);
            HermesBuffer_set(buffer, i, c);
            break;
        }
                           // Blob n'est pas accepté ici
        default:
            break;
        }

    }

    va_end(arg);

    HermesBufferManager_markAsReady(&hermes->output, buffer);

}

HermesBuffer* Hermes_initTX(Hermes_t* hermes) {

    HermesBuffer *buffer = HermesBufferManager_allocBuffer(&hermes->output);
    if (buffer == NULL) {
        return buffer;
    }
    buffer->head.sender = hermes->id;

    return buffer;

}

void Hermes_send(Hermes_t* hermes, HermesBuffer* buffer) {

    uint8_t idx = HermesBufferManager_indexOf(&hermes->output, buffer);
    if (idx != HERMES_BUFFER_COUNT) {
        hermes->output.states[idx].ready = true;
        buffer->head.sender = hermes->id;
        return;
    }

    HermesBuffer* out = HermesBufferManager_allocBuffer(&hermes->output);
    if (buffer == NULL) {
        return;
    }

    out->head = buffer->head;
    out->head.sender = hermes->id;
    out->length = buffer->length;
    memcpy(out->data, buffer->data, out->length);

    HermesBufferManager_markAsReady(&hermes->output, out);

}

void Hermes_handle(Hermes_t* hermes) {

    // Process inputs 
    Hermes_handleInputs(hermes);

    // Process outputs
    Hermes_handleOutputs(hermes);

}

void Hermes_handleInputs(Hermes_t* hermes) {

    uint8_t count = HermesBufferManager_readyBufferCount(&hermes->input);
    uint8_t idx;            // Indice du buffer d'entrée
    HermesBuffer* buffer;   // Buffer d'entrée

    for (; count > 0; count--) {

        idx = HermesBufferManager_nextBuffer(&hermes->input);
        buffer = &hermes->input.buffers[idx];

        if (buffer->head.command >= HermesSysCmd_PartialBegin) {
            // Invalide
            HermesBufferManager_freeFirstBuffer(&hermes->input);
            continue;
        }

#ifndef HERMES_NO_AUTO_HEARTBEAT
        else if (
            buffer->head.command == HermesSysCmd_Heartbeat && 
            buffer->head.remote == hermes->id && 
            !buffer->head.req_res
        ) {
            Hermes_heartbeat(hermes, buffer->head.sender, true);
        } else 
#endif

        if (
            buffer->head.command == HermesSysCmd_Log &&
            hermes->logCallback != NULL
        ) {
            uint8_t error;
            char message[HERMES_MAX_BUFFER_LEN - 2u];

            HermesBuffer_get(buffer, 0, &error);
            HermesBuffer_get(buffer, 1, message);

            hermes->logCallback(hermes, buffer->head, message, error);
        }

        if (buffer->head.command >= HermesSysCmd) {
            // Commande système
            if (hermes->onSystemMessage != NULL) {
                hermes->onSystemMessage(hermes, buffer);
            }
            HermesBufferManager_freeFirstBuffer(&hermes->input);
            continue;
        }

        else if (hermes->onMessage != NULL) {
            hermes->onMessage(hermes, buffer);
        }

        HermesBufferManager_freeFirstBuffer(&hermes->input);

    }

}

void Hermes_handleOutputs(Hermes_t* hermes) {

    if (hermes->send == NULL) {
        return;
    }

    HermesPayload payload = Hermes_output(
        hermes, hermes->defaultPayloadLength
    );

    while (HermesPayload_valid(&payload)) {
        hermes->send(hermes, payload);
        payload = Hermes_output(
            hermes, hermes->defaultPayloadLength
        );
    }

}

void Hermes_log(
    Hermes_t *hermes, HermesID id, 
    const char *message, uint8_t error
) {

    Hermes_respond(hermes, id, HermesSysCmd_Log, "os", error, message);

}

void Hermes_heartbeat(Hermes_t* hermes, HermesID id, bool req_res) {

    HermesBuffer* buf = Hermes_initTX(hermes);
    if (!buf) return; // Erreur silencieuse
    HermesBuffer_configure(buf, id, HermesSysCmd_Heartbeat, req_res, "");
    Hermes_send(hermes, buf);

}