#include "HermesBridge.h"
#include <string.h>

void HermesBridge_init (HermesBridge* bridge, Hermes_t* hermes) {

    bridge->hermes = hermes;
    
    for (uint8_t i = 0; i < 4; i++) {
        bridge->outputs[i].enabled = false;
    }

    for (uint16_t i = 0; i < HERMES_BUFFER_COUNT; i++) {
        bridge->inputSources[i] = 4;
        bridge->outputSources[i] = 4;
    }

}

void HermesBridge_input (
    HermesBridge* bridge, HermesPayload payload, uint8_t source
) {

    Hermes_t* hermes = bridge->hermes;
    uint8_t before = HermesBufferManager_allocatedBufferCount(&hermes->input);
    
    Hermes_input(bridge->hermes, payload);

    uint8_t after = HermesBufferManager_allocatedBufferCount(&hermes->input);

    // Un nouveau buffer a été alloué
    if (before != after) {
        
        uint8_t idx, t;
        HermesBufferManager *manager = &hermes->input;

        // On cherche à savoir si c'est un buffer prioritaire 
        HermesCommand cmd = ((*(uint32_t*)payload.data) >> 9) & 0xfff;

        // Cas spécifique - C'est un PartialBegin
        if (cmd == HermesSysCmd_PartialBegin) {
            cmd = *(uint16_t*)(payload.data + sizeof(uint32_t) + 1);
        }

        if (cmd >= HermesSysCmd) {
            t = manager->aTail;
        } else {
            // Comment faire -1 avec un modulo en C ...
            t = (manager->aHead + HERMES_BUFFER_COUNT - 1) % HERMES_BUFFER_COUNT;
        }

        // On récupère l'index du buffer
        idx = manager->allocated[t];

        // On marque la provenance
        bridge->inputSources[idx] = source;

        // Recherche de la taille minimale
        uint16_t chunkSize = HERMES_MAX_PAYLOAD;
        for (uint8_t i = 0; i < 4; i++) {
            if (i == source || !bridge->outputs[i].enabled) {
                // On ignore la longueur de la source, on ne va pas lui
                // renvoyer
                continue;
            }
            if (chunkSize > bridge->outputs[i].chunkSize) {
                chunkSize = bridge->outputs[i].chunkSize;
            }
        }

        bridge->inputSizes[idx] = chunkSize;

    }

}


void HermesBridge_handle (HermesBridge* bridge) {

    Hermes_t* hermes = bridge->hermes;

    // ENTREE
    uint8_t count = HermesBufferManager_readyBufferCount(&hermes->input);

    // Pour tout buffer d'entrée prêt
    for (; count; count--) {

        // On récupère l'index du prochain buffer à déplacer
        uint8_t id = HermesBufferManager_nextBuffer(&hermes->input);

        // On essaie d'allouer un buffer de sortie
        HermesBuffer* dest = HermesBufferManager_allocBuffer(&hermes->output);
        if (!dest) {
            // On attend le prochain appel
            break;
        }

        // On copie tout le bazard
        memcpy(dest, &hermes->input.buffers[id], sizeof(HermesBuffer));
        uint8_t idx = HermesBufferManager_indexOf(&hermes->output, dest);
        bridge->outputSources[idx] = bridge->inputSources[id];
        bridge->outputSizes[idx] = bridge->inputSizes[id];
        hermes->output.states[idx].ready = true;

        // On libère l'entrée
        HermesBufferManager_freeFirstBuffer(&hermes->input);

    }
    
    // SORTIE
    // On récupère l'index du prochain buffer à partir
    uint8_t id = HermesBufferManager_nextBuffer(&hermes->output);

    // Pour tout buffer de sortie prêt
    while (id != HERMES_BUFFER_COUNT) {

        // On récupère la source de ce buffer
        uint8_t src = bridge->outputSources[id];

        // On récupère la taille de segment associée
        uint16_t chunkSize = bridge->outputSizes[id];

        // Envoi sur chaque support
        HermesPayload payload = Hermes_output(bridge->hermes, chunkSize);
        for (uint8_t i = 0; i < 4; i++) {
            if (i == src || !bridge->outputs[i].enabled) {
                // On ignore la longueur de la source, on ne va pas lui
                // renvoyer
                continue;
            }
            bridge->outputs[i].callback(bridge->outputs[i].slot, payload);
        }

        // On récupère l'index du prochain buffer à partir
        id = HermesBufferManager_nextBuffer(&hermes->output);
    }

}