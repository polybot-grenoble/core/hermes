#include "HermesBufferManager.h"

void HermesBufferManager_init (HermesBufferManager *manager) {

    manager->aHead = 0;
    manager->aTail = 0;

    for (uint8_t i = 0; i < HERMES_BUFFER_COUNT; i++) {
        // Nettoyage des buffers à l'initialisation
        HermesBuffer_clear(&manager->buffers[i]);
        // HERMES_BUFFER_COUNT est invalide
        manager->allocated[i] = HERMES_BUFFER_COUNT;
        // 0 <= key <= 3; 4 est invalide
        manager->states[i].key = 4; 
        // !ready && rw_pos == MAX => buffer libre
        manager->states[i].ready = false; 
        manager->states[i].rw_pos = HERMES_MAX_BUFFER_LEN; 
        manager->states[i].segment = 0;
    }

}

uint8_t HermesBufferManager_allocatedBufferCount (HermesBufferManager *manager) {

    // Cas "circulaire"
    if (manager->aHead < manager->aTail) {
        return (HERMES_BUFFER_COUNT - manager->aTail) + manager->aHead;
    }

    // Cas normal
    return manager->aHead - manager->aTail;

}

uint8_t HermesBufferManager_freeBufferCount (HermesBufferManager *manager) {

    // Cas "circulaire"
    if (manager->aHead < manager->aTail) {
        return manager->aTail - manager->aHead;
    }

    // Cas normal
    return HERMES_BUFFER_COUNT - (manager->aHead - manager->aTail);

}

uint8_t HermesBufferManager_readyBufferCount (HermesBufferManager *manager) {

    uint8_t cnt = 0;

    for (uint8_t i = 0; i < HERMES_BUFFER_COUNT; i++) {
        cnt += manager->states[i].ready;
    } 

    return cnt;

}

HermesBuffer* HermesBufferManager_allocBuffer (HermesBufferManager *manager) {

    // On vérifie qu'on peut ajouter en tête de liste
    uint8_t next = (manager->aHead + 1) % HERMES_BUFFER_COUNT;
    if (next == manager->aTail) {
        // Non
        return NULL;
    }

    // On cherche un buffer libre
    uint8_t i;
    for (i = 0; i < HERMES_BUFFER_COUNT; i++) {
        if (
            !manager->states[i].ready && 
            manager->states[i].rw_pos == HERMES_MAX_BUFFER_LEN
        ) break;
    }

    // Alors, ça devrait JAMAIS arriver, mais au cas où ...
    if (i >= HERMES_BUFFER_COUNT) {
        // Ca dégage !
        return NULL;
    }

    // On marque le buffer comme utilisé
    manager->states[i].rw_pos = 0;
    manager->allocated[manager->aHead] = i;
    manager->aHead = next;

    return &manager->buffers[i];

}

HermesBuffer* HermesBufferManager_allocPrioritizedBuffer (
    HermesBufferManager *manager
) {

    // On vérifie qu'on peut ajouter en fin de liste
    uint8_t prev = (
            manager->aTail + HERMES_BUFFER_COUNT - 1
        ) % HERMES_BUFFER_COUNT;

    if (prev == manager->aHead) {
        // Non
        return NULL;
    }

    // On cherche un buffer libre
    uint8_t i;
    for (i = 0; i < HERMES_BUFFER_COUNT; i++) {
        if (
            !manager->states[i].ready && 
            manager->states[i].rw_pos == HERMES_MAX_BUFFER_LEN
        ) break;
    }

    // Alors, ça devrait JAMAIS arriver, mais au cas où ...
    if (i >= HERMES_BUFFER_COUNT) {
        // Ca dégage !
        return NULL;
    }

    // On marque le buffer comme utilisé
    manager->states[i].rw_pos = 0;
    manager->aTail = prev;
    manager->allocated[manager->aTail] = i;

    return &manager->buffers[i];

}

uint8_t HermesBufferManager_indexOf (
    HermesBufferManager *manager, HermesBuffer* buffer
) {

    for (uint8_t i = 0; i < HERMES_BUFFER_COUNT; i++) {
        if (&manager->buffers[i] == buffer) {
            return i;
        }
    }

    return HERMES_BUFFER_COUNT;

}

void HermesBufferManager_freeLastBuffer (HermesBufferManager *manager) {

    if (manager->aTail == manager->aHead) {
        // Vide
        return;
    }

    uint8_t t = (manager->aHead + HERMES_BUFFER_COUNT - 1) % HERMES_BUFFER_COUNT;
    uint8_t last = manager->allocated[t];

    HermesBuffer_clear(&manager->buffers[last]);
    manager->states[last].key = 4;
    manager->states[last].ready = false;
    manager->states[last].rw_pos = HERMES_MAX_BUFFER_LEN;
    manager->states[last].segment = 0;

    manager->aHead = t;
    manager->allocated[manager->aHead] = HERMES_BUFFER_COUNT;

}

void HermesBufferManager_freeFirstBuffer (HermesBufferManager *manager) {

    if (manager->aTail == manager->aHead) {
        // Vide
        return;
    }

    uint8_t first = manager->allocated[manager->aTail];

    HermesBuffer_clear(&manager->buffers[first]);
    manager->states[first].key = 4;
    manager->states[first].ready = false;
    manager->states[first].rw_pos = HERMES_MAX_BUFFER_LEN;
    manager->states[first].segment = 0;

    manager->allocated[manager->aTail] = HERMES_BUFFER_COUNT;
    manager->aTail = (manager->aTail + 1) % HERMES_BUFFER_COUNT;

}

void HermesBufferManager_freeBuffer (
    HermesBufferManager *manager, HermesBuffer* buffer
) {

    uint8_t index = HermesBufferManager_indexOf(manager, buffer);
    if (index == HERMES_BUFFER_COUNT) {
        return;
    }

    uint8_t first = manager->allocated[manager->aTail];
    uint8_t t = (manager->aHead + HERMES_BUFFER_COUNT - 1) % HERMES_BUFFER_COUNT;
    uint8_t last = manager->allocated[t];
    
    // Si c'est en tête ou en queue, c'est simple
    if (index == last) {
        HermesBufferManager_freeLastBuffer(manager);
        return;
    } else if (index == first) {
        HermesBufferManager_freeFirstBuffer(manager);
        return;
    }

    // Sinon, on doit réorganiser la fifo ... chiantos ...
    bool move = false;
    uint8_t j, k;
    for (j = manager->aTail; j != manager->aHead; j = k) {
        k = (j + 1) % HERMES_BUFFER_COUNT;
        move = move || manager->allocated[j] == index;
        if (move) {
            manager->allocated[j] = manager->allocated[k];
        }
    }
    manager->aHead = t;
    manager->allocated[manager->aHead] = HERMES_BUFFER_COUNT;

    // On libère le buffer
    HermesBuffer_clear(buffer);
    manager->states[index].ready = false;
    manager->states[index].rw_pos = HERMES_MAX_BUFFER_LEN;
    manager->states[index].key = 4;
    manager->states[index].segment = 0;

}

uint8_t HermesBufferManager_nextBuffer (HermesBufferManager *manager) {

    uint8_t j, k, idx;
    for (j = manager->aTail; j != manager->aHead; j = k) {
        k = (j + 1) % HERMES_BUFFER_COUNT;
        idx = manager->allocated[j];
        if (manager->states[idx].ready) { return idx; }
    }

    return HERMES_BUFFER_COUNT;

}

uint8_t HermesBufferManager_findRXBuffer (
    HermesBufferManager *manager,
    HermesID sender, HermesCommand command
) {

    // On parcourt dans l'ordre de la fifo
    uint8_t j, k, idx;
    for (j = manager->aTail; j != manager->aHead; j = k) {
        k = (j + 1) % HERMES_BUFFER_COUNT;
        idx = manager->allocated[j];
        if (
            manager->buffers[idx].head.sender == sender &&
            manager->buffers[idx].head.command == command
        ) { return idx; }
    }

    // On a rien trouvé
    return HERMES_BUFFER_COUNT;

}

uint8_t HermesBufferManager_findTXBuffer (
    HermesBufferManager *manager,
    HermesID remote, HermesCommand command
) {
    
    // On parcourt dans l'ordre de la fifo
    uint8_t j, k, idx;
    for (j = manager->aTail; j != manager->aHead; j = k) {
        k = (j + 1) % HERMES_BUFFER_COUNT;
        idx = manager->allocated[j];
        if (
            manager->buffers[idx].head.remote == remote &&
            manager->buffers[idx].head.command == command
        ) { return idx; }
    }

    // On a rien trouvé
    return HERMES_BUFFER_COUNT;

}

uint8_t HermesBufferManager_findRXPartial (
    HermesBufferManager *manager,
    HermesID sender, HermesCommand command,
    uint8_t key
) {

    // On parcourt dans l'ordre de la fifo
    uint8_t j, k, idx;
    for (j = manager->aTail; j != manager->aHead; j = k) {
        k = (j + 1) % HERMES_BUFFER_COUNT;
        idx = manager->allocated[j];
        if (
            manager->buffers[idx].head.sender == sender &&
            manager->buffers[idx].head.command == command &&
            manager->states[idx].key == key
        ) { return idx; }
    }

    // On a rien trouvé
    return HERMES_BUFFER_COUNT;

}

void HermesBufferManager_markAsReady (
    HermesBufferManager *manager, 
    HermesBuffer *buffer
) {

    uint8_t idx = HermesBufferManager_indexOf(manager, buffer);
    if (idx >= HERMES_BUFFER_COUNT) {
        return;
    }

    manager->states[idx].ready = true;

}