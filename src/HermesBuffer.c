#include "HermesBuffer.h"

#include <string.h>
#include <stddef.h>

HermesBufferID HermesHeader_serialize (HermesHeader header) {

    return (
        (header.remote  << 21) |
        (header.command <<  9) |
        (header.sender  <<  1) |
        (header.req_res <<  0) 
    );

}

HermesHeader HermesHeader_parse (HermesBufferID id) {

    HermesHeader h;

    h.req_res   = id & 0x001;
    id >>=  1;
    h.sender    = id & 0x0ff;
    id >>=  8;
    h.command   = id & 0xfff;
    id >>= 12;
    h.remote    = id & 0x0ff;

    return h;

}

void HermesBuffer_clear (HermesBuffer *buffer) {

    buffer->head.remote     = 0;
    buffer->head.command    = 0;
    buffer->head.sender     = 0;
    buffer->head.req_res    = false;

    buffer->length = 0;
    buffer->data[0] = HermesT_none;

}

void HermesBuffer_setSignature (HermesBuffer *buffer, const char* signature) {

    uint8_t *loc = buffer->data;
    const char* s = signature;
    char c;

    for (c = *s; c && c != HermesT_blob; (c = *(++s))) {
        *loc = c;
        // Si c est invalide, il sera réécrit silencieusemment après
        loc = HermesType_next(loc);
    }

    // On finit le buffer avec HermesT_none
    *loc = c;
    if (c == HermesT_blob) {
        loc++;
        *loc = HermesT_none;
    }

    // On calcule la longueur du buffer 
    // sans tenir compte de HermesT_none.
    // HermesT_none sera ajouté à la réception !
    buffer->length = (loc - buffer->data);

}

void HermesBuffer_configure (
    HermesBuffer *buffer, 
    HermesID remote, HermesCommand command, bool req_res,
    const char* signature
) {

    buffer->head.remote = remote;
    buffer->head.command = command;
    buffer->head.req_res = req_res;
    HermesBuffer_setSignature(buffer, signature);

}

uint16_t HermesBuffer_argCount (HermesBuffer *buffer) {

    uint8_t *loc;
    uint16_t n = 0;

    for (
        loc = buffer->data; 
        (*loc != HermesT_none && *loc != HermesT_blob) 
            && n < HERMES_MAX_BUFFER_LEN;
        (loc = HermesType_next(loc))
    ) n++;
    
    if (*loc == HermesT_blob) {
        n++;
    }

    return n;

}

void HermesBuffer_getSignature (HermesBuffer *buffer, char *signature) {

    uint8_t *loc;
    uint16_t n = 0;
    char *c = signature;

    for (
        loc = buffer->data; 
        (*loc != HermesT_none && *loc != HermesT_blob) 
            && n < HERMES_MAX_BUFFER_LEN;
        (loc = HermesType_next(loc))
    ) {
        *c = *loc;
        c++;
    };
    
    if (*loc == HermesT_blob) {
        *c = *loc;
        c++;
    }

    *c = 0;

}

uint8_t* HermesBuffer_dataEnd (HermesBuffer* buffer) {

    return &buffer->data[buffer->length];

}

uint8_t* HermesBuffer_argLoc (HermesBuffer* buffer, uint16_t index) {
    
    uint8_t *loc;
    uint16_t n = 0;

    for (
        loc = buffer->data; 
        (*loc != HermesT_none && *loc != HermesT_blob) 
            && n < index && n < HERMES_MAX_BUFFER_LEN;
        (loc = HermesType_next(loc))
    ) n++;

    if (n != index) {
        return HermesBuffer_dataEnd(buffer);
    }

    return loc;

}

void HermesBuffer_shiftArgument (
    HermesBuffer *buffer, 
    uint16_t index, int16_t shift
) {

    // Rien à faire -> Ne fait rien !
    if (shift == 0) return;

    // Calculs intermédiaires 
    uint8_t *arg = HermesBuffer_argLoc(buffer, index);
    uint8_t *end = HermesBuffer_dataEnd(buffer);
    
    uint8_t *neo = arg + shift;
    uint8_t *neo_end = end + shift;

    // On met à jour la taille du buffer
    buffer->length += shift;

    // Pas d'argument -> On bouge juste HermesT_none !
    if (arg == end) {
        *neo_end = HermesT_none;
        return;
    }

    // Il y a des trucs à bouger
    if (shift > 0) {

        // Déplacement à droite -> on commence par la fin
        *neo_end = HermesT_none;
        uint8_t *loc = end - 1, *neo_loc = neo_end - 1;
        
        for (; loc >= arg; loc--) {
            *neo_loc = *loc;
            neo_loc--;
        } 
        return;

    } else {

        // Déplacement à gauche -> on commence par le début
        uint8_t *loc = arg, *neo_loc = neo;
        
        for (; loc < end; loc++) {
            *neo_loc = *loc;
            neo_loc++;
        } 

        *neo_end = HermesT_none;
        return;
        
    }

}

bool HermesBuffer_set (HermesBuffer* buffer, uint16_t index, const void* argument) {

    uint8_t *loc = HermesBuffer_argLoc(buffer, index);
    
    if (loc == HermesBuffer_dataEnd(buffer)) {
        return false;
    }

    if (*loc == HermesT_blob) {
        return false;
    }

    uint8_t *data = loc + 1;

    if (*loc == HermesT_string) {
        // Calcul du décalage nécessaire
        size_t actual = strlen((const char*)data);
        size_t future = strlen((char*)argument);
        ptrdiff_t offset = future - actual;

        // Décalage
        HermesBuffer_shiftArgument(buffer, index + 1, offset);

        // Copie
        size_t s = (future + 1) * sizeof(char); // +1 pour copier le 0
        memcpy(data, argument, s);

        return true;
    }

    // Calcul de la taille de la donnée
    size_t s = HermesType_size(*loc);
    
    // Copie
    memcpy(data, argument, s);

    return true;

}

bool HermesBuffer_get (HermesBuffer* buffer, uint16_t index, void* argument) {

    uint8_t *loc = HermesBuffer_argLoc(buffer, index);
    
    if (loc == HermesBuffer_dataEnd(buffer)) {
        return false;
    }

    if (*loc == HermesT_blob) {
        return false;
    }

    uint8_t *data = loc + 1;

    if (*loc == HermesT_string) {
        size_t s = (1 + strlen(data)) * sizeof(char);
        memcpy(argument, data, s);
        
        return true;
    }

    size_t s = HermesType_size(*loc);
    memcpy(argument, data, s);

    return true;

}

bool HermesBuffer_setBlob (
    HermesBuffer* buffer, uint16_t index,
    size_t len, void* data
) {

    uint8_t *loc = HermesBuffer_argLoc(buffer, index);
    uint8_t *arg = loc + 1;
    uint8_t *end = HermesBuffer_dataEnd(buffer);

    if (loc == end) {
        return false;
    }

    if (*loc != HermesT_blob) {
        return false;
    }

    size_t offset = arg - buffer->data;
    buffer->length = offset + len;

    if (!len) {
        return true;
    }

    memcpy(arg, data, len);
    *HermesBuffer_dataEnd(buffer) = HermesT_none;

    return true;
}

bool HermesBuffer_getBlob (
    HermesBuffer* buffer, uint16_t index,
    size_t *len, void* data
) {

    uint8_t *loc = HermesBuffer_argLoc(buffer, index);
    uint8_t *arg = loc + 1;
    uint8_t *end = HermesBuffer_dataEnd(buffer);

    if (loc == end) {
        return false;
    }

    if (*loc != HermesT_blob) {
        return false;
    }

    *len = end - arg; 

    if (!*len) {
        return true;
    }

    memcpy(data, arg, *len);

    return true;

}

bool HermesPayload_valid (HermesPayload *payload) {
    uint32_t id = *(uint32_t*)payload->data;
    return (payload->length >= 4) && id && !(id >> 29);
}

bool HermesPayload_validFiltered (HermesPayload *payload, uint8_t self) {
    uint32_t id = *(uint32_t*)payload->data;
    return (
        (payload->length >= 4)        && 
        (id != 0)                     &&
        !(id >> 29)                   && 
        (((id >> 21) & 0xff) == self) 
    );
}
