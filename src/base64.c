#include "base64.h"
#include <stdlib.h>

// Conversion logic
const char *Base64LUT = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
const char Base64Filler = 64;

/**
 * @brief Converts a base64 character to its associated 6-bit value
 * @param c Character to convert
 * @return Associated 6-bits value
 */
static uint8_t Base64ToBits(const char c) {

	if ('A' <= c && c <= 'Z') {
		return c - 'A';
	}

	if ('a' <= c && c <= 'z') {
		return c - 'a' + 26;
	}

	if ('0' <= c && c <= '9') {
		return c - '0' + 52;
	}

	if (c == '+') {
		return 62;
	}

	if (c == '/') {
		return 63;
	}

	// Only handles '=' if used correctly
	return 64;
}

// Exposed code

size_t Base64StringLength(size_t len) {
	return 4 * ((len / 3) + (len % 3 != 0));
}

size_t maxBlobFromBase64(size_t len) {
	return 3 * (len / 4);
}

void blobToBase64(char* dest, const uint8_t* data, size_t len) {
	// Implemented from :
	// https://fr.wikipedia.org/wiki/Base64#Description
	
	// Do not use NULL as a string ;)
	if (!dest || !data) return;

	// Compute how many 24-bit groups are present in the array
	size_t groups = len / 3;
	uint8_t remainder = len % 3;

	// Fill the string with the regular bits
	size_t i = 0, k;
	uint8_t a, b, c, d;

	for (k = 0; k < groups; k++) {
		// Computing indexes
		a = (data[3 * k + 0] >> 2) & 0x3f;
		b = ((data[3 * k + 0] & 0x3) << 4) | ((data[3 * k + 1] >> 4) & 0xf);
		c = ((data[3 * k + 1] & 0xf) << 2) | ((data[3 * k + 2] >> 6) & 0x3);
		d = data[3 * k + 2] & 0x3f;
		
		// Adding to the string using the LUT
		dest[i++] = Base64LUT[a];
		dest[i++] = Base64LUT[b];
		dest[i++] = Base64LUT[c];
		dest[i++] = Base64LUT[d];
	}

	// Fill the string with the remaining bits
	if (remainder) {
		// Max 2 bytes => d is '='
		d = Base64Filler;

		// At least 1 element
		a = (data[3 * k + 0] >> 2) & 0x3f;
		b = ((data[3 * k + 0] & 0x3) << 4); // Zero-padded

		if (remainder == 2) {
			// 2 bytes
			b |= ((data[3 * k + 1] >> 4) & 0xf);
			c = ((data[3 * k + 1] & 0xf) << 2); // Zero-padded
		}
		else {
			// Only 1 byte => c is '='
			c = Base64Filler;
		}

		// Adding to the string using the LUT
		dest[i++] = Base64LUT[a];
		dest[i++] = Base64LUT[b];
		dest[i++] = Base64LUT[c];
		dest[i++] = Base64LUT[d];
	}

	// End the sting with 0
	dest[i] = 0;

}

size_t Base64ToBlob(uint8_t* data, const char* str) {

	// Reject NULL
	if (!str || !data) return 0;

	// Get string length
	size_t len = strlen(str);
	
	// Decoding data
	size_t k = 0, i;
	uint8_t a, b, c, d;
	uint8_t fillerCount = 0;
	uint32_t t;

	for (i = 0; i < len; i += 4) {
		// Get values
		a = Base64ToBits(str[i + 0]);
		b = Base64ToBits(str[i + 1]);
		c = Base64ToBits(str[i + 2]);
		d = Base64ToBits(str[i + 3]);

		t = (a << (6 * 3)) | (b << (6 * 2)) | (c << 6) | d;

		// Set buffer values
		data[k++] = (t >> 16) & 0xff;
		data[k++] = (t >> 8) & 0xff;
		data[k++] = t & 0xff;

		// Count '=' symbols
		fillerCount += (c == Base64Filler) + (d == Base64Filler);
	}

	// 0 <= fillerCount <= 2
	return k - fillerCount;

}

/// HermesBuffer - Base64

size_t Base64SerializedPayloadLen(const HermesPayload* payload)
{
	return Base64StringLength(payload->length);
}

void HermesPayloadToBase64(char* dest, const HermesPayload* payload)
{
	blobToBase64(dest, payload->data, payload->length);
}

HermesPayload Base64ToHermesPayload(const char* str)
{
	HermesPayload payload;
	payload.length = Base64ToBlob((uint8_t*)&payload.data, str);
	return payload;
}