#include "HermesType.h"

#include <string.h>

size_t HermesType_size (HermesType type) {

    switch (type)
    {
    case HermesT_char:
        return sizeof(char);
        
    case HermesT_int8:
        return sizeof(int8_t);
        
    case HermesT_int16:
        return sizeof(int16_t);

    case HermesT_int32:
        return sizeof(int32_t);

    case HermesT_int64:
        return sizeof(int64_t);
    
    case HermesT_float:
        return sizeof(float);
    
    case HermesT_double:
        return sizeof(double);
        
    case HermesT_string:
        return sizeof(char);
        
    default:
        return 0;
    }

}

size_t HermesType_length(uint8_t *data) {

    HermesType t = *data;

    size_t len = HermesType_size(t);
    
    if (t == HermesT_string) {
        const char *str = (const char*)(data + 1);
        len += strlen(str);
    }

    return len;

}

uint8_t *HermesType_next(uint8_t *data) {

    HermesType t = *data;
    size_t len = HermesType_length(data);

    // Blob est invalide, None aussi
    if (t == HermesT_none || t == HermesT_blob) {
        return data;
    }

    return data + (1 + len);

}