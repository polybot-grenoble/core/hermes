/**
 * @file Hermes.h
 * @author Julien PIERSON
 * @brief Fichier principal d'Hermes C.
 * 
 * Inclue les définitions de :
 *  - `Hermes_t` la structure Hermès
 *  - Fonction de réception standard 
 *  - Fonction d'envoi standard
 *  - Fonctions d'intéractions Hermès-Utilisateur
 * 
 * @version 2.0
 * @date 2024-10-27 
 *
 *     __  __                                 _    _____ 
 *    / / / /__  _________ ___  ___  _____   | |  / /__ \
 *   / /_/ / _ \/ ___/ __ `__ \/ _ \/ ___/   | | / /__/ /
 *  / __  /  __/ /  / / / / / /  __(__  )    | |/ // __/ 
 * /_/ /_/\___/_/  /_/ /_/ /_/\___/____/     |___//____/ 
 *                                                       
 */

#ifndef __HERMES_H__
#define __HERMES_H__

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

#include "Constants.h"
#include "HermesBuffer.h"
#include "HermesBufferManager.h"
#include "peripheral.h"

/**
 * @brief Structure Hermès. Contient tous les éléments permettant l'utilisation
 * du protocole sur la couche choisie.
 */
typedef struct sHermes {

    /* Public */

    /// Identifiant périphérique
    uint8_t id;

    /// Longueur par défaut d'une charge utile
    uint16_t defaultPayloadLength;

    /// Gestionnaire des buffer d'entrée
    HermesBufferManager input;

    /// Gestionnaire des buffer de sortie
    HermesBufferManager output;

    /// Gestionnaire des définitions du périphérique
    HermesPeripheral peripheral;

    /// Fonction appelée lors de la réception d'un buffer "log". Si le code 
    /// d'erreur n'est pas nul, alors il s'agit d'un message d'erreur. 
    void (*logCallback)(
        struct sHermes* hermes, 
        HermesHeader head, const char* message, uint8_t errorCode
    );

    /* Interaction integrateur */

    /// Fonction appelée lorsqu'Hermès veut envoyer des données
    void (*send)(struct sHermes* hermes, HermesPayload payload);

    /* Interaction utilisateur */

    /// Fonction appelée à la réception d'un buffer utilisateur.
    void (*onMessage)(struct sHermes* hermes, HermesBuffer* buffer);

    /// Fonction appelée à la réception d'un buffer système.
    void (*onSystemMessage)(struct sHermes* hermes, HermesBuffer* buffer);

    /* Private */

    /// Dernière clé produite pour la transmission partielle
    uint8_t partialKey;

    /// Emplacement pour adapter Hermès à un environnement orienté-objet
    void* slot;

} Hermes_t;

// -- Initialisation d'Hermès -- //

/**
 * @brief Initialise Hermès
 * 
 * @param hermes Hermès
 * @param id Identifiant périphérique
 */
void Hermes_init (Hermes_t *hermes, HermesID id);


// -- Interactions Hermès-Support -- //

/**
 * @brief Récupère et traite le fragment. N'effectue pas de vérification 
 * sur la validité du fragment et ne filtre pas sur l'identifiant de récepteur. 
 * Ces vérifications doivent être effectués en amont. 
 * 
 * Cette fonction retourne faux si il n'y a pas de buffer disponible. 
 * 
 * @param hermes Hermès
 * @param payload Le fragment à traiter
 * @return true Le fragment a été traité.
 * @return false Il n'y a plus de place disponible. 
 */
bool Hermes_input (Hermes_t *hermes, HermesPayload payload);

/**
 * @brief Donne un fragment à envoyer sur le support.
 * Si il n'y a rien à envoyer, le fragment sera invalide. Merci d'utiliser 
 * `HermesPayload_valid` pour effectuer cette vérification.
 * 
 * @param hermes Hermès
 * @param maxLen Longueur maximale autorisée sur le support. 
 *               Ne peut pas dépasser `HERMES_MAX_PAYLOAD`.
 * 
 * @return HermesPayload Les octets à envoyer
 */
HermesPayload Hermes_output (Hermes_t *hermes, uint16_t maxLen);


// -- Interactions Hermès-Utilisateur -- //

/**
 * @brief Envoie une requête à un périphérique. NON BLOQUANT.
 * 
 * @param hermes Hermès
 * @param remote Identifiant du récepteur
 * @param cmd Identifiant de commande
 * @param signature Signature des arguments
 * @param ... arguments (passés par copie)
 * @return true Si l'envoi est fait, false Si les buffer de sortie sont pleins.
 */
bool Hermes_request (
    Hermes_t* hermes, 
    HermesID remote, HermesCommand cmd, const char* signature,
    ...
);

/**
 * @brief Envoie une réponse à un périphérique. NON BLOQUANT.
 *
 * @param hermes Hermès
 * @param remote Identifiant du récepteur
 * @param cmd Identifiant de commande
 * @param signature Signature des arguments
 * @param ... arguments (passés par copie)
 * @return true Si l'envoi est fait, false Si les buffer de sortie sont pleins.
 */
bool Hermes_respond (
    Hermes_t* hermes,
    HermesID remote, HermesCommand cmd, const char* signature,
    ...
);

/**
 * @brief Alloue un buffer de sortie et le donne à l'utilisateur.
 * 
 * @param hermes Hermès
 * @return Un buffer si trouvé, NULL si plein.
 */
HermesBuffer* Hermes_initTX (Hermes_t* hermes);

/**
 * @brief Valide l'envoi d'un buffer. Si le buffer n'a pas été alloué
 * via `Hermes_initTX`, le contenu est copié dans un buffer de sortie 
 * et envoyé. Attention : si aucun buffer de sortie n'est disponible, 
 * rien n'est indiqué et le message part aux oubliettes.
 * 
 * @param hermes Hermès
 * @param buffer Le buffer à envoyer
 */
void Hermes_send (Hermes_t *hermes, HermesBuffer *buffer);


// -- Fonctions de traitement Hermès -- //

/**
 * @brief Traites les entrées et les sorties. [A développer ...]
 * 
 * @param hermes Hermès
 */
void Hermes_handle (Hermes_t* hermes);

/**
 * @brief Traite les buffer entrants. Si un buffer est prêt,
 * Hermès appelle le callback de gestion approprié.
 * 
 * @param hermes Hermès
 */
void Hermes_handleInputs (Hermes_t* hermes);

/**
 * @brief Traite les buffers sortants. Si un buffer est prêt,
 * Hermès appelle le callback d'envoi. 
 * 
 * @param hermes 
 */
void Hermes_handleOutputs (Hermes_t* hermes);


// -- Raccourcis d'utilisation -- //

/**
 * @brief Envoie un buffer de type "log" au périphérique indiqué.
 * Ce paquet sera marqué comme "réponse". 
 * 
 * @param hermes Hermès
 * @param id Destinataire
 * @param message Message à afficher chez le destinataire
 * @param error Code d'erreur. 0 si ce n'est pas une erreur.
 */
void Hermes_log (
    Hermes_t* hermes, HermesID id, 
    const char* message, uint8_t error
);

/**
 * @brief Envoie un paquet heartbeat à l'adresse indiquée
 * 
 * @param hermes Hermès
 * @param id Le destinataire
 * @param req_res Drapeau requête/réponse
 */
void Hermes_heartbeat(Hermes_t* hermes, HermesID id, bool req_res);

#endif // !__HERMES_H__
