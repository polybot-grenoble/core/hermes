#ifndef __HERMES_PERIPHERAL_METHODS__
#define __HERMES_PERIPHERAL_METHODS__

#include "Hermes.h"
#include <stdlib.h>

/** 
 * ========== Définition ============
 *  Manifeste de périphérique Hermès
 * ==================================
 *
 * - Représentation compacte en binaire des méthodes définies pour 
 *   un périphérique.
 * 
 * On a, dans l'ordre :
 * (? correspond à une longueur variable, minimum : 1)
 * 
 * [En-tête] (4 octets)
 * Longueur du manifeste    (2 octets)
 * Nombre de méthodes       (2 octets)
 * 
 * [Contenu]
 * Famille du périphérique  (? octets)
 * Méthode 1                (? octets)
 * ...
 * Méthode N                (? octets)
 * 
 * 
 * Format d'une méthode :
 * ID                       (2 octets)
 * Nom                      (? octets)
 * Signature entrante       (? octets)
 * Signature sortante       (? octets)
 * 
 */
typedef struct {
    // Longueur du manifeste
    uint16_t length;
    // Nombre de méthodes
    uint16_t methods;
    // Le manifeste à envoyer
    uint8_t* manifest;
} HermesManifest;

/**
 * @brief Initialise le périphérique Hermès. Doit être appelé après Hermes_init.
 * 
 * @param hermes 
 * @param handlers 
 * @param handlerCount 
 */
void HermesPeripheral_setup (
    Hermes_t* hermes, 
    const char* name,
    const HermesHandler* handlers, 
    uint16_t handlerCount
);

/**
 * @brief Gère l'arrivée d'un buffer selon le modèle du périphérique
 * 
 * @param peripheral 
 * @param buffer 
 */
void HermesPeripheral_handle (
    Hermes_t* peripheral,  
    HermesBuffer* buffer
);

/**
 * @brief Crée un manifeste à envoyer. 
 * Attention ! Utilise l'allocation dynamique.
 * 
 * @param hermes Hermès
 * @return HermesManifest* Le manifeste
 */
HermesManifest HermesPeripheral_manifest (Hermes_t* hermes);

void HermesPeripheral_sendManifest (Hermes_t* hermes, HermesBuffer* buffer);

#endif