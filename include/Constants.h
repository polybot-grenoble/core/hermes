/**
 * @file Constants.h
 * @author Julien PIERSON
 * @brief Fichier définissant les constantes utilisées par Hermès
 * 
 * Inclue les définitions de :
 *  - Taille maximale d'un buffer
 *  - Nombre de buffers / gestionnaire
 *  - Nombre de commandes / périphérique
 *  - Nombre d'arguments / commande de périphérique
 *  - Raccourcis de types 
 *  - Erreurs
 * 
 * @version 2.0
 * @date 2024-10-27 
 *
 *     __  __                                 _    _____ 
 *    / / / /__  _________ ___  ___  _____   | |  / /__ \
 *   / /_/ / _ \/ ___/ __ `__ \/ _ \/ ___/   | | / /__/ /
 *  / __  /  __/ /  / / / / / /  __(__  )    | |/ // __/ 
 * /_/ /_/\___/_/  /_/ /_/ /_/\___/____/     |___//____/ 
 *                                                       
 */

#ifndef __HERMES_CONSTANTS__
#define __HERMES_CONSTANTS__

#include <stdint.h>
#include <stdlib.h>

#ifndef HERMES_MAX_BUFFER_LEN 
#define HERMES_MAX_BUFFER_LEN 1000u
#endif // !HERMES_MAX_BUFFER_LEN 

#ifndef HERMES_MAX_PAYLOAD
#define HERMES_MAX_PAYLOAD 12u
#endif

#ifndef HERMES_BUFFER_COUNT
#define HERMES_BUFFER_COUNT 8U
#endif

typedef uint8_t  HermesID;
typedef uint16_t HermesCommand;

enum HermesSystemCmd {
    HermesSysCmd_PartialContent = 0xFFC,
    HermesSysCmd_PartialBegin   = 0xFFB,
    HermesSysCmd_Heartbeat      = 0xFFA,
    HermesSysCmd_Error          = 0xFF9,
    HermesSysCmd_Log            = 0xFF8,
    HermesSysCmd_Manifest       = 0xFF7,
    HermesSysCmd_ManifestChunk  = 0xFF6,
    HermesSysCmd                = 0xFF0
};

#endif // !__HERMES_CONSTANTS__