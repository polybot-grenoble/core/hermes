#ifndef __HERMES_PERIPHERAL__
#define __HERMES_PERIPHERAL__

#include <stdint.h>
#include "Constants.h"
#include "HermesBuffer.h"

typedef void (*HermesMethod)(void*, HermesBuffer*);

typedef struct {
    
    /** Identifiant de commande */
    HermesCommand command;

    /** Nom de la commande */
    const char* name;

    /** Signature d'arguments attendue */
    const char* signature;
    
    /** Callback lié à la commande */
    HermesMethod callback;

    /** Signature d'arguments de réponse */
    const char* outSignature;
    
    /** Drapeau requête/réponse */
    bool response;

    /** [En cas de réponse] Identifiant de l'envoyeur */
    HermesID from;

} HermesHandler;

typedef struct {

    /** Famille du périphérique */
    const char* name;

    /** Nombre de gestionnaires */
    uint16_t handlerCount;

    /** Liste des gestionnaires */
    const HermesHandler* handlers;

    /** Nombre de gestionnaires pour le système */
    uint16_t sysHandlerCount;

    /** Listes des gestionnaires pour le système */
    const HermesHandler* sysHandlers;

} HermesPeripheral;



#endif
