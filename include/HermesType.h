/**
 * @file HermesType.h
 * @author Julien PIERSON
 * @brief Fichier définissant les types d'arguments utilisé dans les buffers.
 * 
 * Inclue les définitions de :
 *  - Types d'arguments
 *  - Calcul de taille (minimale) d'argument
 *  - Calcul de longueur mémoire d'argument
 * 
 * @version 2.0
 * @date 2024-10-29 
 *
 *     __  __                                 _    _____ 
 *    / / / /__  _________ ___  ___  _____   | |  / /__ \
 *   / /_/ / _ \/ ___/ __ `__ \/ _ \/ ___/   | | / /__/ /
 *  / __  /  __/ /  / / / / / /  __(__  )    | |/ // __/ 
 * /_/ /_/\___/_/  /_/ /_/ /_/\___/____/     |___//____/ 
 *                                                       
 */

#ifndef __HERMES_TYPE__
#define __HERMES_TYPE__

#include <stdlib.h>
#include <stdint.h>

/**
 * Indicateurs de type d'argument Hermès
 */
typedef enum {
    
    HermesT_none    = 0,
    HermesT_char    = 'c',
    
    HermesT_int8    = 'o',
    HermesT_int16   = 'x',
    HermesT_int32   = 'i',
    HermesT_int64   = 'l',
    
    HermesT_float   = 'f',
    HermesT_double  = 'd',

    HermesT_string  = 's',
    HermesT_blob    = 'b',

} HermesType;

/**
 * @brief Donne la taille (minimale) d'un argument dans un buffer 
 * 
 * @param type Type d'argument
 * @return size_t Taille minimale
 */
size_t HermesType_size (HermesType type);

/**
 * @brief Calcule la taille d'un argument présent dans un buffer. *data doit
 * pointer vers l'indicateur de type.
 * 
 * Ce calcul ne fonctionne pas dans le cas d'un blob, retourne 0.
 * 
 * @param data Indicateur de type
 * @return size_t Taille de l'argument (sans compter l'indicateur)
 */
size_t HermesType_length(uint8_t *data);

/**
 * @brief Calcule la position du prochain argument dans le buffer. *data doit
 * pointer vers l'indicateur de type. 
 * 
 * Ce calcul ne fonctionne pas dans le cas d'un blob, retourne data.
 * 
 * @param data Indicateur de type
 * @return uint8_t* Emplacement du prochain argument
 */
uint8_t *HermesType_next(uint8_t *data);

#endif