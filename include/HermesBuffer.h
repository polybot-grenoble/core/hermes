/**
 * @file HermesBuffer.h
 * @author Julien PIERSON
 * @brief Fichier définissant les buffers par Hermès
 * 
 * Inclue les définitions de :
 *  - `HermesHeader` l'en-tête de buffer
 *  - `HermesBuffer` le buffer
 *  - Fonctions de manipulation des en-têtes
 *  - Fonctions de manipulation des arguments
 * 
 * @version 2.0
 * @date 2024-10-27 
 *
 *     __  __                                 _    _____ 
 *    / / / /__  _________ ___  ___  _____   | |  / /__ \
 *   / /_/ / _ \/ ___/ __ `__ \/ _ \/ ___/   | | / /__/ /
 *  / __  /  __/ /  / / / / / /  __(__  )    | |/ // __/ 
 * /_/ /_/\___/_/  /_/ /_/ /_/\___/____/     |___//____/ 
 *                                                       
 */

#ifndef __HERMES_BUFFER__
#define __HERMES_BUFFER__

#include <stdint.h>
#include <stdbool.h>

#include "Constants.h"
#include "HermesType.h"

/**
 * En-tête de buffer Hermès
 */
typedef struct {
    HermesID        remote;     /// ID du périphérique destinataire
    HermesCommand   command;    /// ID de la commande
    HermesID        sender;     /// ID de l'envoyeur
    bool            req_res;    /// Drapeau Requête/Réponse
} HermesHeader;

/** Identifiant 29-bits représentant un HermesHeader */
typedef uint32_t HermesBufferID;

/**
 * @brief Sérialise une en-tête en identifiant 29-bits
 * 
 * @param header En-tête
 * @return uint32_t Identifiant 29-bits
 */
HermesBufferID HermesHeader_serialize (HermesHeader header);

/**
 * @brief Décompose un identifiant 29-bits en en-tête
 * 
 * @param id Identifiant 29-bits
 * @return HermesHeader En-tête
 */
HermesHeader HermesHeader_parse (HermesBufferID id);

/**
 * Buffer Hermès
 */
typedef struct {
    // En-tête du buffer
    HermesHeader head; 
    // Longueur des données du buffer
    uint16_t length;   
    // Données du buffer
    uint8_t data[HERMES_MAX_BUFFER_LEN];
} HermesBuffer;

/** 
 * @brief Met un buffer à zéro 
 * @param buffer le buffer
 */
void HermesBuffer_clear (HermesBuffer *buffer);

/**
 * @brief Définit les indicateurs de type dans le buffer. Ne nettoie pas les
 * données présentes, attention aux poubelles !
 * 
 * @param buffer Buffer à configurer
 * @param signature Signature à appliquer
 */
void HermesBuffer_setSignature (HermesBuffer *buffer, const char* signature);

/**
 * @brief Configure un buffer
 * 
 * @param buffer Buffer à configurer
 * @param remote Destinataire
 * @param command N° de commande
 * @param req_res Drapeau Requête/Réponse
 * @param signature Signature des arguments contenus dans le buffer
 */
void HermesBuffer_configure (
    HermesBuffer *buffer, 
    HermesID remote, HermesCommand command, bool req_res,
    const char* signature
);

/**
 * @brief Compte le nombre d'arguments dans le buffer
 * 
 * @param buffer Le buffer
 * @return Nombre d'arguments
 */
uint16_t HermesBuffer_argCount (HermesBuffer *buffer);

/**
 * @brief Calcule la signature du buffer.
 * 
 * @param buffer Le buffer
 * @param signature La signature calculée
 */
void HermesBuffer_getSignature (HermesBuffer *buffer, char *signature);

/**
 * @brief Retourne l'adresse de la fin du champ data du buffer.
 * 
 * @param buffer Buffer
 * @return uint8_t* Fin du champ data
 */
uint8_t* HermesBuffer_dataEnd (HermesBuffer* buffer);

/**
 * @brief Retourne la position mémoire d'un argument selon son indice. *data
 * pointe sur l'indicateur de type.
 * 
 * Si l'indice est invalide, retourne `HermesBuffer_dataEnd(buffer)`.
 * 
 * @param buffer Le buffer
 * @param index Indice de l'argument
 * @return uint8_t* Emplacement mémoire de l'argument
 */
uint8_t* HermesBuffer_argLoc (HermesBuffer* buffer, uint16_t index);

/**
 * @brief Déplace un argument dans le buffer d'un montant donné.
 * Un déplacement positif va "vers la droite", négatif "vers la gauche". 
 * 
 * @param buffer Le buffer
 * @param index L'indice de l'argument
 * @param shift Le décalage
 */
void HermesBuffer_shiftArgument (
    HermesBuffer *buffer, 
    uint16_t index, int16_t shift
);

/**
 * @brief Définit la valeur d'un argument dans le buffer. 
 * Ne fonctionne pas avec les blobs ! Il faut utiliser `HermesBuffer_setBlob`.
 * 
 * @param buffer Le buffer
 * @param index Indice de l'argument
 * @param argument Pointeur vers la valeur à copier
 * @return true L'argument a été défini
 * @return false Index invalide, ou l'argument est un blob
 */
bool HermesBuffer_set (HermesBuffer* buffer, uint16_t index, const void* argument);

/**
 * @brief Lit la valeur d'un argument dans le buffer. 
 * Ne fonctionne pas avec les blobs ! Il faut utiliser `HermesBuffer_getBlob`.
 * 
 * @param buffer Le buffer
 * @param index Indice de l'argument
 * @param argument Pointeur vers l'emplacement où stocker la valeur
 * @return true L'argument a été lu
 * @return false Index invalide, ou l'argument est un blob
 */
bool HermesBuffer_get (HermesBuffer* buffer, uint16_t index, void* argument);

/**
 * @brief Définit la valeur d'un blob
 * 
 * @param buffer Buffer
 * @param index Indice de l'argument
 * @param len Longueur des données à copier (en octets)
 * @param data Données à copier
 * @return true L'argument a été défini
 * @return false Index invalide, ou pas un blob
 */
bool HermesBuffer_setBlob (
    HermesBuffer* buffer, uint16_t index,
    size_t len, void* data
);

/**
 * @brief Lit la valeur d'un blob
 * 
 * @param buffer Buffer
 * @param index Indice de l'argument
 * @param len Longueur des données copiées (en octets)
 * @param data Stockage où copier
 * @return true L'argument a été lu
 * @return false Index invalide, ou pas un blob
 */
bool HermesBuffer_getBlob (
    HermesBuffer* buffer, uint16_t index,
    size_t *len, void* data
);

/**
 * Format intermédiaire entre le support et Hermès. 
 * Aussi appelé "Fragment de données". Les 4 premiers octets sont l'identifiant
 * du buffer associé codé sur 32 bits (petit-boutiste). 
 */
typedef struct {
    uint16_t length;
    uint8_t data[HERMES_MAX_PAYLOAD];
} HermesPayload;

/**
 * @brief Vérifie que le format est correcte :
 * 
 *  - Longueur >= 4
 * 
 *  - Bits 29 à 31 = 0
 * 
 *  - ID != 0
 * 
 * @param payload 
 * @return true Le format est correcte.
 * @return false Le format est incorrect
 */
bool HermesPayload_valid (HermesPayload *payload);

/**
 * @brief Vérifie que le format est correcte :
 * 
 * - Longueur >= 4
 * 
 * - Bits 29 à 31 d'ID = 0
 * 
 * - ID >> 21 = self
 * 
 * - ID != 0
 * 
 * @param payload 
 * @param self ID du périphérique courant
 * @return true Le format est correcte.
 * @return false Le format est incorrect
 */
bool HermesPayload_validFiltered (HermesPayload *payload, uint8_t self);



#endif // !__HERMES_BUFFER__