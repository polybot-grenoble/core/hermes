/**
 * @file HermesBufferManager.h
 * @author Julien PIERSON
 * @brief Fichier définissant le gestionnaire de buffers Hermès
 * 
 * Inclue les définitions de :
 *  - `HermesBufferState` état d'un buffer géré par Hermès
 *  - `HermesBufferManager` le gestionnaire de buffers
 * 
 * @version 2.0
 * @date 2024-11-01 
 *
 *     __  __                                 _    _____ 
 *    / / / /__  _________ ___  ___  _____   | |  / /__ \
 *   / /_/ / _ \/ ___/ __ `__ \/ _ \/ ___/   | | / /__/ /
 *  / __  /  __/ /  / / / / / /  __(__  )    | |/ // __/ 
 * /_/ /_/\___/_/  /_/ /_/ /_/\___/____/     |___//____/ 
 *                                                       
 */


#ifndef __HERMES_BUFFER_MANAGER__
#define __HERMES_BUFFER_MANAGER__

#include "Constants.h"
#include "HermesBuffer.h"

/** Etat d'un buffer Hermès */
typedef struct {
    
    // Position de la tête de lecture/écriture
    uint16_t rw_pos;

    // Drapeau signifiant un buffer prêt
    bool ready;

    // Clé de transmission partielle
    uint8_t key;

    // Longueur d'un segment reçu par transmission partielle
    uint8_t segment;

} HermesBufferState;

/**
 * @brief Gestionnaire de Buffer Hermès
 */
typedef struct
{
    /* Buffers gérés */
    HermesBuffer buffers[HERMES_BUFFER_COUNT];

    /**
     * @brief Liste d'ordonnancement des buffers gérés.
     * 
     * - aHead pointe toujours une case vide.
     * 
     * - aTail pointe toujours une case pleine, à moins d'être supperposé à
     *   aHead, dès lors aTail pointe une case vide. 
     */
    uint8_t allocated[HERMES_BUFFER_COUNT];
    
    /* Indice de tête de liste */    
    uint8_t aHead;
    /* Indice de queue de liste */
    uint8_t aTail;

    /** Etats des buffers gérés */
    HermesBufferState states[HERMES_BUFFER_COUNT];

} HermesBufferManager;

/**
 * @brief Initialise un gestionnaire de buffers
 * 
 * @param manager Le gestionnaire
 */
void HermesBufferManager_init (HermesBufferManager *manager);

/**
 * @brief Compte le nombre de buffers alloués
 * 
 * @param manager Le gestionnaire
 */
uint8_t HermesBufferManager_allocatedBufferCount (HermesBufferManager *manager);

/**
 * @brief Compte le nombre de buffers libres
 * 
 * @param manager Le gestionnaire
 */
uint8_t HermesBufferManager_freeBufferCount (HermesBufferManager *manager);

/**
 * @brief Compte le nombre de buffers prêts 
 * 
 * @param manager Le gestionnaire
 */
uint8_t HermesBufferManager_readyBufferCount (HermesBufferManager *manager);

/**
 * @brief Alloue un buffer et retourne son pointeur. Si aucun buffer n'est 
 * disponible, la fonction retourne NULL.
 * 
 * @param manager Le gestionnaire
 * @return HermesBuffer* Le pointeur vers le buffer (ou NULL).
 */
HermesBuffer* HermesBufferManager_allocBuffer (HermesBufferManager *manager);

/**
 * @brief Alloue un buffer prioritaire et retourne son pointeur. 
 * Si aucun buffer n'est disponible, la fonction retourne NULL.
 * 
 * @param manager Le gestionnaire
 * @return HermesBuffer* Le pointeur vers le buffer (ou NULL).
 */
HermesBuffer* HermesBufferManager_allocPrioritizedBuffer (
    HermesBufferManager *manager
);

/**
 * @brief Donne l'indice correspondant à un pointeur d'un buffer.
 * Si le buffer n'existe pas dans le gestionnaire, la fonction retourne 
 * HERMES_BUFFER_COUNT.
 * 
 * @param manager Le gestionnaire
 * @param buffer Le buffer
 * @return uint8_t L'indice du buffer dans le gestionnaire.
 */
uint8_t HermesBufferManager_indexOf (
    HermesBufferManager *manager, HermesBuffer* buffer
);

/**
 * @brief Libère le dernier buffer de la file d'attente.
 * 
 * @param manager Le gestionnaire
 */
void HermesBufferManager_freeLastBuffer (HermesBufferManager *manager);

/**
 * @brief Libère le premier buffer de la file d'attente.
 * 
 * @param manager Le gestionnaire
 */
void HermesBufferManager_freeFirstBuffer (HermesBufferManager *manager);

/**
 * @brief Libère un buffer associé à un gestionnaire.
 * 
 * @param manager Le gestionnaire
 * @param buffer Le buffer
 */
void HermesBufferManager_freeBuffer (
    HermesBufferManager *manager, HermesBuffer* buffer
);

/**
 * @brief Donne l'indice du premier buffer prêt. Si aucun buffer n'est prêt, 
 * la fonction renvoie HERMES_BUFFER_COUNT.
 * 
 * @param manager Le gestionnaire
 * @return uint8_t L'indice du premier buffer prêt.
 */
uint8_t HermesBufferManager_nextBuffer (HermesBufferManager *manager);

/**
 * @brief Donne l'indice du buffer correspondant aux filtres. Si aucun buffer 
 * ne correspond, la fonction renvoie HERMES_BUFFER_COUNT.
 * 
 * @param manager Le gestionnaire
 * @param sender ID de l'envoyeur
 * @param command ID de la commande
 * @return uint8_t Indice du buffer
 */
uint8_t HermesBufferManager_findRXBuffer (
    HermesBufferManager *manager,
    HermesID sender, HermesCommand command
);

/**
 * @brief Donne l'indice du buffer correspondant aux filtres. Si aucun buffer 
 * ne correspond, la fonction renvoie HERMES_BUFFER_COUNT.
 * 
 * @param manager Le gestionnaire
 * @param remote ID du destinataire
 * @param command ID de la commande
 * @return uint8_t Indice du buffer
 */
uint8_t HermesBufferManager_findTXBuffer (
    HermesBufferManager *manager,
    HermesID remote, HermesCommand command
);

/**
 * @brief Donne l'indice du buffer correspondant aux filtres. Si aucun buffer 
 * ne correspond, la fonction renvoie HERMES_BUFFER_COUNT.
 * 
 * @param manager Le gestionnaire
 * @param sender ID de l'envoyeur
 * @param command ID de la commande
 * @param key Clé de transmission partielle
 * @return uint8_t Indice du buffer
 */
uint8_t HermesBufferManager_findRXPartial (
    HermesBufferManager *manager,
    HermesID sender, HermesCommand command,
    uint8_t key
);

/**
 * @brief Marque un buffer géré comme étant prêt
 * 
 * @param manager Le gestionnaire
 * @param buffer Le buffer
 */
void HermesBufferManager_markAsReady (
    HermesBufferManager *manager, 
    HermesBuffer *buffer
);

#endif
