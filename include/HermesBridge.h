#ifndef __HERMES__BRIDGE__
#define __HERMES__BRIDGE__

#include "Hermes.h"

/** Type de callback pour HermesBridge */
typedef void (*HermesBridgeCallback)(void*, HermesPayload);

typedef struct {

    /** Indique si le support est actif */
    bool enabled;

    /** Taille maximale d'un paquet pour le support */
    uint16_t chunkSize;

    /** Fonction appelée pour transmettre sur le support */
    HermesBridgeCallback callback;

    /** Argument passé au callback */
    void* slot;

} HermesSupport;

/** Permet l'interconnexion d'au plus 4 supports */
typedef struct {

    /** Instance d'Hermès servant de tampon */
    Hermes_t *hermes;

    /** Sorties */
    HermesSupport outputs[4];

    /** Tableaux des provenances des paquets */
    uint8_t inputSources[HERMES_BUFFER_COUNT];

    /** Tableaux des tailles de sortie */
    uint8_t inputSizes[HERMES_BUFFER_COUNT];

    /** Tableaux des provenances des paquets */
    uint8_t outputSources[HERMES_BUFFER_COUNT];

    /** Tableaux des tailles de sortie */
    uint8_t outputSizes[HERMES_BUFFER_COUNT];

} HermesBridge;

/**
 * @brief Initialise l'objet HermesBridge
 * 
 * @param bridge Le pont
 * @param hermes Instance d'Hermès à utiliser
 */
void HermesBridge_init (HermesBridge* bridge, Hermes_t* hermes);

/**
 * @brief Importe un fragment de données dans Hermès
 * 
 * @param bridge Le pont
 * @param payload Le fragment
 * @param source L'identifiant de source
 */
void HermesBridge_input (
    HermesBridge* bridge, HermesPayload payload, uint8_t source
);

/**
 * @brief Distribue des charges utiles sur les différents supports
 */
void HermesBridge_handle (HermesBridge* bridge);

#endif