#ifndef __HERMES_BASE64__
#define __HERMES_BASE64__

#include "Constants.h"
#include "HermesBuffer.h"
#include "Hermes.h"
#include <stdint.h>
#include <string.h>

/**
 * @brief Computes the length of a base64-encoded string built from
 * a byte array.
 * 
 * @param len Byte array length
 * @return Base64-encoded string length (does not include C-string ending 0 !)
 */
size_t Base64StringLength(size_t len);

/**
 * @brief Computes the maximum byte array length from a base64-encoded
 * string.
 * 
 * @param len String length
 * @return Maximum byte array size
 */
size_t maxBlobFromBase64(size_t len);

/**
 * @brief Serializes a byte array into a C string with base64 encoding.
 * The output string must have enough memory allocated 
 * to accomodate the result. 
 * 
 * @param dest String to store the base64 encoded byte array. 
 * @param data Byte array
 * @param len Array length
 */
void blobToBase64(char* dest, const uint8_t* data, size_t len);

/**
 * @brief Deserializes a base64-encoded string into a byte array. 
 * The output array must have enough memory allocated
 * to accommodate the result.
 *
 * @param data Output array
 * @param str Base64-encoded string
 * @return Size of deserialized data
 */
size_t Base64ToBlob(uint8_t* data, const char* str);

/// HermesPayload - Base64 conversions

/**
 * @brief Computes the length of a base64-encoded string built from
 * an HermesBuffer.
 * @param payload Payload to get the length from
 * @return Base64-encoded string length (does not include C-string ending 0 !)
 */
size_t Base64SerializedPayloadLen(const HermesPayload* payload);

/**
 * @brief Serializes an HermesBuffer to a base64-encoded string
 * 
 * @param dest String to store the encoded buffer
 * @param payload Payload to encode
 */
void HermesPayloadToBase64(char* dest, const HermesPayload* payload);

/**
 * @brief Deserializes a base64-encoded string to an HermesBuffer. 
 * Does not check for string length nor anything that could add
 * protection to the code. Maybe it will be added soon™.
 *
 * @param str Base64-encoded string
 * @return The payload
 */
HermesPayload Base64ToHermesPayload(const char *str);

#endif